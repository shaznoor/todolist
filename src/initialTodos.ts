import { Todo } from './type';

export const initialTodos: Array<Todo> = [
  {
    text: 'Walk the dog',
    complete: false,
  },
];
